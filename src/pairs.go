package main

import (
	"net/http"
	"io/ioutil"
	//"fmt"
)

func get_pair_list(url string, pairs * []string) {
	df, err := http.Get(url)
	if err != nil {
		panic(err.Error())
	}
	res, err := ioutil.ReadAll(df.Body)
	if err != nil {
		panic(err.Error())
	}
	*pairs = []string(res)
	//fmt.Println(res)
}


func get_pairs(mapURI map[string]string, Pair_symbol map[string]string)  {
	bitfinex_pairs(mapURI, Pair_symbol)
	//okcoin_pairs(mapURI, Pair_symbol)
	//hitbtc_pairs(mapURI, Pair_symbol)
	//bitstamp_pairs(mapURI, Pair_symbol)
	//yobit_pairs(mapURI, Pair_symbol)
}

func bitfinex_pairs(mapURI map[string]string, Pair_symbol map[string]string) {
	market := "bitfinex"
	var pairsList []string
	uri := mapURI[market] + Pair_symbol[market]
	get_pair_list(uri, &pairsList)
	bitfinex_tikcer(mapURI[market], pairsList)
}
//
//func okcoin_pairs(mapURI map[string]string, Pair_symbol map[string]string) {
//
//}
//
//func hitbtc_pairs(mapURI map[string]string, Pair_symbol map[string]string) {
//
//}
//
//func bitstamp_pairs(mapURI map[string]string, Pair_symbol map[string]string) {
//
//}
//
//func yobit_pairs(mapURI map[string]string, Pair_symbol map[string]string) {
//
//}