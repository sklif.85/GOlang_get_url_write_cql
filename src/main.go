
package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	//"net"
)

type Config struct {
	Markets markets
	Scylla  scylla
}

type markets struct {
	Markets_name []string
	Markets_uri  map[string]string
	Pair_symbol  map[string]string
}

type scylla struct {
	Nodes []string
	Port  int
}

func get_config(conf *Config) {
	configeFile := "config"
	_, err := toml.DecodeFile(configeFile, &conf)
	if err != nil {
		fmt.Println("You don't have CONFIG file, nemed - ", configeFile)
	}
}

func main() {
	var config Config
	get_config(&config)
	get_pairs(config.Markets.Markets_uri, config.Markets.Pair_symbol)
}
